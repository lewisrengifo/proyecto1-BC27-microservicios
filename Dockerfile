FROM openjdk:8
VOLUME /tmp
EXPOSE 8081
ADD ./target/proyecto1-0.0.1-SNAPSHOT.jar proyecto1.jar
ENTRYPOINT ["java", "-jar", "/proyecto1.jar"]