package com.bootcamp;

import com.bootcamp.application.BankAccountController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class Proyecto1ApplicationTests {

  @Autowired private BankAccountController bankAccountController;

  @Test
  void contextLoads() throws Exception {
    assertThat(bankAccountController).isNotNull();
  }
}
