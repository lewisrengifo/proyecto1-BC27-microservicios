package com.bootcamp.domain;

import com.bootcamp.infraestructure.document.BankAccounts;
import com.bootcamp.infraestructure.repository.BankAccountRepository;
import com.bootcamp.infraestructure.service.BankAccountsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BankAccountsServiceImplTest {
  @Mock
  BankAccountRepository bankAccountRepository;

  BankAccountsService bankAccountsService;

  @BeforeEach
  void initUseCase() {
    bankAccountsService = new BankAccountsServiceImpl();
  }

  @Test
  void getAll() {

    BankAccounts BankAccounts = new BankAccounts();
    when(bankAccountRepository.findAll()).thenReturn(Flux.just(BankAccounts));
    verify(bankAccountRepository, times(1)).findAll();
  }

  @Test
  void saveSavings() {}

  @Test
  void saveChecking() {}

  @Test
  void retirements() {}

  @Test
  void deposits() {}

  @Test
  void registerRetirements() {}

  @Test
  void registerDeposits() {}

  @Test
  void deleteById() {}

  @Test
  void saveFixedTerms() {}

  @Test
  void findById() {}

  @Test
  void existById() {}
}
