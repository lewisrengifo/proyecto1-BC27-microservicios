package com.bootcamp.application;

import com.bootcamp.infraestructure.document.BankAccounts;
import com.bootcamp.infraestructure.entity.Holders;
import com.bootcamp.infraestructure.repository.BankAccountRepository;
import com.bootcamp.domain.BankAccountsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = BankAccountController.class)
// @Import(BankAccountsServiceImpl.class)
class BankAccountControllerTest {
  @MockBean
  BankAccountRepository bankAccountRepository;

  @MockBean BankAccountsServiceImpl bankAccountsService;
  @Autowired private WebTestClient webTestClient;

  @BeforeEach
  void setUp() {}

  @Test
  void registerSavings() {
    BankAccounts bankAccounts = new BankAccounts();
    bankAccounts.setTypeOfBankAccount("ahorros");
    BigDecimal totalAmount = new BigDecimal("189.09");
    bankAccounts.setTotalAmount(totalAmount);
    bankAccounts.setClientDocument("7896969");
    bankAccounts.setClientName("lewis");
    bankAccounts.setId("1");
    bankAccounts.setTypeOfClient("empresarial");
    ArrayList<Holders> mocklistHolders = new ArrayList<>();
    Holders mockHolders = new Holders();
    mockHolders.setNameHolders("lewis");
    mockHolders.setSignatories("lewis");
    mocklistHolders.add(mockHolders);
    bankAccounts.setHoldersList(mocklistHolders);
    bankAccounts.setMaintenanceFee(totalAmount);
    bankAccounts.setTotalTransactionsMonth(1);
    // BankAccounts mock2BankAccount = bankAccounts;
    Mono<BankAccounts> bankAccountsMono = Mono.just(bankAccounts);
    // Mockito.when(bankAccountRepository.save(bankAccounts)).thenReturn(Mono.just(bankAccounts));
    when(bankAccountsService.saveSavings(bankAccounts)).thenReturn(bankAccountsMono);
    webTestClient
        .post()
        .uri("/bankAccount/savings")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .body(bankAccountsMono, BankAccounts.class)
        .exchange()
        .expectStatus()
        .isCreated();

    verify(bankAccountsService, Mockito.times(1))
        .saveSavings(ArgumentMatchers.any(BankAccounts.class));
  }

  @Test
  void registerChecking() {
    BankAccounts bankAccounts = new BankAccounts();
    bankAccounts.setTypeOfBankAccount("ahorros");
    BigDecimal totalAmount = new BigDecimal("189.09");
    bankAccounts.setTotalAmount(totalAmount);
    bankAccounts.setClientDocument("7896969");
    bankAccounts.setClientName("lewis");
    bankAccounts.setId("1");
    bankAccounts.setTypeOfClient("empresarial");
    ArrayList<Holders> mocklistHolders = new ArrayList<>();
    Holders mockHolders = new Holders();
    mockHolders.setNameHolders("lewis");
    mockHolders.setSignatories("lewis");
    mocklistHolders.add(mockHolders);
    bankAccounts.setHoldersList(mocklistHolders);
    bankAccounts.setMaintenanceFee(totalAmount);
    bankAccounts.setTotalTransactionsMonth(1);
    Mono<BankAccounts> bankAccountsMono = Mono.just(bankAccounts);
    when(bankAccountsService.saveChecking(bankAccounts)).thenReturn(bankAccountsMono);
    webTestClient
        .post()
        .uri("/bankAccount/checkings")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .body(bankAccountsMono, BankAccounts.class)
        .exchange()
        .expectStatus()
        .isCreated();
    verify(bankAccountsService, Mockito.times(1))
        .saveChecking(ArgumentMatchers.any(BankAccounts.class));
  }

  @Test
  void registerRetirement() {
    String id = "2";
    BankAccounts bankAccounts = new BankAccounts();
    bankAccounts.setTypeOfBankAccount("ahorros");
    BigDecimal totalAmount = new BigDecimal("189.09");
    bankAccounts.setTotalAmount(totalAmount);
    bankAccounts.setClientDocument("7896969");
    bankAccounts.setClientName("lewis");
    bankAccounts.setId("2");
    bankAccounts.setTypeOfClient("empresarial");
    ArrayList<Holders> mocklistHolders = new ArrayList<>();
    Holders mockHolders = new Holders();
    mockHolders.setNameHolders("lewis");
    mockHolders.setSignatories("lewis");
    mocklistHolders.add(mockHolders);
    bankAccounts.setHoldersList(mocklistHolders);
    bankAccounts.setMaintenanceFee(totalAmount);
    bankAccounts.setTotalTransactionsMonth(1);
    Mono<BankAccounts> bankAccountsMono = Mono.just(bankAccounts);
    when(bankAccountsService.registerRetirements(id, bankAccounts)).thenReturn(bankAccountsMono);
    webTestClient
        .put()
        .uri("/bankAccount/retirement/2")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .body(bankAccountsMono, BankAccounts.class)
        .exchange()
        .expectStatus()
        .isOk();
    verify(bankAccountsService, Mockito.times(1))
        .registerRetirements(
            ArgumentMatchers.any(String.class), ArgumentMatchers.any(BankAccounts.class));
  }

  @Test
  void registerDeposits() {
    String id = "2";
    BankAccounts bankAccounts = new BankAccounts();
    bankAccounts.setTypeOfBankAccount("ahorros");
    BigDecimal totalAmount = new BigDecimal("189.09");
    bankAccounts.setTotalAmount(totalAmount);
    bankAccounts.setClientDocument("7896969");
    bankAccounts.setClientName("lewis");
    bankAccounts.setId("2");
    bankAccounts.setTypeOfClient("empresarial");
    ArrayList<Holders> mocklistHolders = new ArrayList<>();
    Holders mockHolders = new Holders();
    mockHolders.setNameHolders("lewis");
    mockHolders.setSignatories("lewis");
    mocklistHolders.add(mockHolders);
    bankAccounts.setHoldersList(mocklistHolders);
    bankAccounts.setMaintenanceFee(totalAmount);
    bankAccounts.setTotalTransactionsMonth(1);
    Mono<BankAccounts> bankAccountsMono = Mono.just(bankAccounts);
    when(bankAccountsService.registerDeposits(id, bankAccounts)).thenReturn(bankAccountsMono);
    webTestClient
        .put()
        .uri("/bankAccount/deposits/2")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .body(bankAccountsMono, BankAccounts.class)
        .exchange()
        .expectStatus()
        .isOk();
    verify(bankAccountsService, Mockito.times(1))
        .registerDeposits(
            ArgumentMatchers.any(String.class), ArgumentMatchers.any(BankAccounts.class));
  }

  @Test
  void readBankAccounts() {

    when(bankAccountsService.getAll()).thenReturn(Flux.just());
    webTestClient.get().uri("/bankAccount/find").exchange().expectStatus().isOk();
    verify(bankAccountsService, Mockito.times(1)).getAll();
  }

  @Test
  void findById() {
    String id = "2";
    BankAccounts bankAccounts = new BankAccounts();
    when(bankAccountsService.findById(id)).thenReturn(Mono.just(bankAccounts));
    webTestClient.get().uri("/bankAccount/findById/2").exchange().expectStatus().isOk();
    verify(bankAccountsService, Mockito.times(1)).findById(ArgumentMatchers.any(String.class));
  }

  @Test
  void deleteById() {
    String id = "2";
    BankAccounts bankAccounts = new BankAccounts();
    when(bankAccountsService.deleteById(id)).thenReturn(Mono.just(bankAccounts));
    webTestClient.delete().uri("/bankAccount/delete/2").exchange().expectStatus().isOk();
    verify(bankAccountsService, Mockito.times(1)).deleteById(ArgumentMatchers.any(String.class));
  }

  @Test
  void saveFixedTerms() {
    BankAccounts bankAccounts = new BankAccounts();
    bankAccounts.setTypeOfBankAccount("ahorros");
    BigDecimal totalAmount = new BigDecimal("189.09");
    bankAccounts.setTotalAmount(totalAmount);
    bankAccounts.setClientDocument("7896969");
    bankAccounts.setClientName("lewis");
    bankAccounts.setId("2");
    bankAccounts.setTypeOfClient("empresarial");
    ArrayList<Holders> mocklistHolders = new ArrayList<>();
    Holders mockHolders = new Holders();
    mockHolders.setNameHolders("lewis");
    mockHolders.setSignatories("lewis");
    mocklistHolders.add(mockHolders);
    bankAccounts.setHoldersList(mocklistHolders);
    bankAccounts.setMaintenanceFee(totalAmount);
    bankAccounts.setTotalTransactionsMonth(1);
    Mono<BankAccounts> bankAccountsMono = Mono.just(bankAccounts);
    when(bankAccountsService.saveFixedTerms(bankAccounts)).thenReturn(bankAccountsMono);
    webTestClient
        .post()
        .uri("/bankAccount/fixedTerms")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .body(bankAccountsMono, BankAccounts.class)
        .exchange()
        .expectStatus()
        .isCreated();
    verify(bankAccountsService, Mockito.times(1))
        .saveFixedTerms(ArgumentMatchers.any(BankAccounts.class));
  }
}
