package com.bootcamp.application;

import com.bootcamp.domain.CreditsServiceImpl;
import com.bootcamp.infraestructure.document.Credits;
import com.bootcamp.infraestructure.entity.CreditCard;
import com.bootcamp.infraestructure.repository.CreditsRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = CreditsController.class)
class CreditsControllerTest {

  @MockBean
  CreditsRepository creditsRepository;
  @MockBean
  CreditsServiceImpl creditsService;

  @Autowired private WebTestClient webTestClient;

  @Test
  void registerCredit() throws ParseException {
    Credits credits = new Credits();
    credits.setId("1");
    credits.setType("personal");
    credits.setClientDocument("70444714");
    BigDecimal totalAmount = BigDecimal.valueOf(14355.44);
    credits.setTotalAmount(totalAmount);
    CreditCard creditCard = new CreditCard();
    creditCard.setCardNumber("11274743747474378");
    String sDate = "08/08/2022";
    Date expDate = new SimpleDateFormat("dd/MM/yyyy").parse(sDate);
    creditCard.setExpirationDate(expDate);
    creditCard.setCcv("676");
    credits.setCreditCard(creditCard);

    Mono<Credits> creditsMono = Mono.just(credits);
    when(creditsService.save(credits)).thenReturn(creditsMono);

    webTestClient
        .post()
        .uri("/credits/save")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .body(creditsMono, Credits.class)
        .exchange()
        .expectStatus()
        .isCreated();
    verify(creditsService, times(1)).save(ArgumentMatchers.any(Credits.class));
  }

  @Test
  void registerPayment() throws ParseException {
    String id = "1";
    Credits credits = new Credits();
    credits.setId("1");
    credits.setType("personal");
    credits.setClientDocument("70444714");
    BigDecimal totalAmount = BigDecimal.valueOf(14355.44);
    credits.setTotalAmount(totalAmount);
    CreditCard creditCard = new CreditCard();
    creditCard.setCardNumber("11274743747474378");
    String sDate = "08/08/2022";
    Date expDate = new SimpleDateFormat("dd/MM/yyyy").parse(sDate);
    creditCard.setExpirationDate(expDate);
    creditCard.setCcv("676");
    credits.setCreditCard(creditCard);

    Mono<Credits> creditsMono = Mono.just(credits);
    when(creditsService.registerPayments(id, credits)).thenReturn(creditsMono);

    webTestClient
        .put()
        .uri("/credits/payments/1")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .body(creditsMono, Credits.class)
        .exchange()
        .expectStatus()
        .isOk();
    verify(creditsService, times(1))
        .registerPayments(ArgumentMatchers.any(String.class), ArgumentMatchers.any(Credits.class));
  }

  @Test
  void registerConsumption() throws ParseException {
    BigDecimal consumption = BigDecimal.valueOf(123.56);
    Credits credits = new Credits();
    credits.setId("1");
    credits.setType("personal");
    credits.setClientDocument("70444714");
    BigDecimal totalAmount = BigDecimal.valueOf(14355.44);
    credits.setTotalAmount(totalAmount);
    CreditCard creditCard = new CreditCard();
    creditCard.setCardNumber("11274743747474378");
    String sDate = "08/08/2022";
    Date expDate = new SimpleDateFormat("dd/MM/yyyy").parse(sDate);
    creditCard.setExpirationDate(expDate);
    creditCard.setCcv("676");
    credits.setCreditCard(creditCard);

    Mono<Credits> creditsMono = Mono.just(credits);

    when(creditsService.registerConsumption(credits, consumption)).thenReturn(creditsMono);

    webTestClient
        .put()
        .uri("/credits/consumption/123.56")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .body(creditsMono, Credits.class)
        .exchange()
        .expectStatus()
        .isOk();

    verify(creditsService, times(1))
        .registerConsumption(
            ArgumentMatchers.any(Credits.class), ArgumentMatchers.any(BigDecimal.class));
  }

  @Test
  void readCredits() {

    when(creditsService.getAll()).thenReturn(Flux.just());
    webTestClient.get().uri("/credits/getAll").exchange().expectStatus().isOk();
    verify(creditsService, times(1)).getAll();
  }

  @Test
  void deleteById() {
    String id = "1";
    Credits credits = new Credits();
    when(creditsService.deleteById(id)).thenReturn(Mono.just(credits));
    webTestClient.delete().uri("/credits/delete/1").exchange().expectStatus().isOk();
    verify(creditsService, times(1)).deleteById(ArgumentMatchers.any(String.class));
  }
}
