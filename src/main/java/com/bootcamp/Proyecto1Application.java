package com.bootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Springboot application for proyecto 1 bootcamp 27 microservices.
 */
@SpringBootApplication
@EnableEurekaClient
public class Proyecto1Application {

  public static void main(String[] args) {
    SpringApplication.run(Proyecto1Application.class, args);
  }
}
