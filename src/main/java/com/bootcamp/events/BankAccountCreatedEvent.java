package com.bootcamp.events;

import com.bootcamp.infraestructure.document.BankAccounts;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class BankAccountCreatedEvent extends Event<BankAccounts>{
}
