package com.bootcamp.infraestructure.document;

import com.bootcamp.infraestructure.entity.CreditCard;

import java.math.BigDecimal;
import javax.validation.constraints.Null;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/** Mongo document for credits. */
@Getter
@Setter
@NoArgsConstructor
@Document(collection = "credits")
public class Credits {

  @Id private String id;
  private String clientDocument;
  private BigDecimal totalAmount;
  private BigDecimal creditLimit;
  private String type;
  @Null private CreditCard creditCard;
}
