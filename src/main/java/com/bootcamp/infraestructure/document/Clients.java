package com.bootcamp.infraestructure.document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Mongo document for clients.
 */
@Getter
@Setter
@NoArgsConstructor
@Document(collection = "clients")
public class Clients {
  @Id private String id;
  private String name;
  private String lastname;
  private String type;
}
