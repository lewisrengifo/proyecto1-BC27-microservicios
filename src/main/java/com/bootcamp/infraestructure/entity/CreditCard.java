package com.bootcamp.infraestructure.entity;

import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Credit card entity. */
@Getter
@Setter
@NoArgsConstructor
public class CreditCard {
  private String cardNumber;
  private Date expirationDate;
  private String ccv;
}
