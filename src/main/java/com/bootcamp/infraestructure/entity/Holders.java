package com.bootcamp.infraestructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entity for holders of bank accounts.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Holders {
  private String nameHolders;
  private String signatories;
}
