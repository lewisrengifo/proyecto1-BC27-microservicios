package com.bootcamp.infraestructure.dto;

import com.bootcamp.infraestructure.document.BankAccounts;

/**
 * BankAccount Request DTO for avoid vulnerability.
 */
public class BankAccountRequest extends BankAccounts {}
