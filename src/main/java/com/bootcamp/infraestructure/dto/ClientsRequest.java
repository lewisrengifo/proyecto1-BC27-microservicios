package com.bootcamp.infraestructure.dto;

import com.bootcamp.infraestructure.document.Clients;

/**
 * ClientRequest DTO for avoid vulnerability.
 */
public class ClientsRequest extends Clients {}
