package com.bootcamp.infraestructure.service;

import com.bootcamp.infraestructure.document.BankAccounts;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service for bank account.
 */
public interface BankAccountsService {

  Flux<BankAccounts> getAll();

  Mono<BankAccounts> saveSavings(BankAccounts bankAccounts);

  Mono<BankAccounts> saveChecking(BankAccounts bankAccounts);

  Mono<BankAccounts> registerRetirements(String clientId, BankAccounts bankAccounts);

  Mono<BankAccounts> registerDeposits(String clientId, BankAccounts bankAccounts);

  Mono<BankAccounts> deleteById(String clientId);

  Mono<BankAccounts> saveFixedTerms(BankAccounts bankAccounts);

  Mono<BankAccounts> findById(String clientId);

  Mono<Boolean> existById(String clientId);
}
