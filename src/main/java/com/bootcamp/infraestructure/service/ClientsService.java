package com.bootcamp.infraestructure.service;

import com.bootcamp.infraestructure.document.Clients;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Interface for clients service.
 */
public interface ClientsService {

  Flux<Clients> getAll();

  Mono<Clients> save(Clients clients);

  Mono<Clients> findById(String id);

  Mono<Boolean> existById(String id);
}
