package com.bootcamp.infraestructure.service;

import com.bootcamp.infraestructure.document.Credits;
import java.math.BigDecimal;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** Credits service interface. */
public interface CreditsService {

  Flux<Credits> getAll();

  Mono<Credits> save(Credits credits);

  Mono<Credits> existByDocumentId(String documentId);

  Mono<Credits> deleteById(String clientId);

  Mono<Credits> registerPayments(String document, Credits credits);

  Mono<Credits> registerConsumption(Credits credits, BigDecimal consumption);

  Mono<Credits> findById(String id);
}
