package com.bootcamp.infraestructure.repository;

import reactor.core.publisher.Mono;

/**
 * CreditsCustomRepository for custom implementations.
 */
public interface CreditsCustomRepository {
  Mono<Boolean> hasCredits(String document);
}
