package com.bootcamp.infraestructure.repository;

import com.bootcamp.infraestructure.document.Clients;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Reactive repository for clients.
 */
@Repository
public interface ClientsRepository extends ReactiveMongoRepository<Clients, String> {
}
