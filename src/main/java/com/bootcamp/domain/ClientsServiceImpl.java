package com.bootcamp.domain;

import com.bootcamp.infraestructure.document.Clients;
import com.bootcamp.infraestructure.repository.ClientsRepository;
import com.bootcamp.infraestructure.service.ClientsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Implementation of client service interface.
 */
@Service
public class ClientsServiceImpl implements ClientsService {

  @Autowired private ClientsRepository clientsRepository;

  @Override
  public Flux<Clients> getAll() {
    return this.clientsRepository.findAll();
  }

  @Override
  public Mono<Clients> save(Clients clients) {
    return this.clientsRepository.save(clients);
  }

  @Override
  public Mono<Clients> findById(String id) {
    return this.clientsRepository.findById(id);
  }

  @Override
  public Mono<Boolean> existById(String id) {
    return this.clientsRepository.existsById(id);
  }
}
