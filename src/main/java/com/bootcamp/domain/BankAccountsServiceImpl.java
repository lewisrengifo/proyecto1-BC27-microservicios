package com.bootcamp.domain;

import com.bootcamp.infraestructure.document.BankAccounts;
import com.bootcamp.infraestructure.entity.Holders;
import com.bootcamp.infraestructure.repository.BankAccountRepository;
import com.bootcamp.infraestructure.service.BankAccountsService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** Implementation for bank account service. */
@Service
public class BankAccountsServiceImpl implements BankAccountsService {

  Logger log = Logger.getLogger("Mylog");

  @Autowired private BankAccountEventsService bankAccountEventsService;

  @Autowired private BankAccountRepository bankAccountRepository;

  @Override
  public Flux<BankAccounts> getAll() {
    return this.bankAccountRepository.findAll();
  }

  @Override
  public Mono<BankAccounts> saveSavings(BankAccounts bankAccounts) {
    Holders holders = bankAccounts.getHoldersList().get(0);

    log.info("lista de holders: " + holders.getNameHolders());
    if (bankAccounts.getTypeOfClient().equals("empresarial")) {
      return Mono.empty();
    } else {
      return bankAccountRepository
          .hasSavingsAccount(bankAccounts.getClientDocument(), "ahorros")
          .flatMap(
              hasSavings -> {
                if (hasSavings) {
                  log.info("El cliente tiene ahorros, no se guarda su cuenta");
                  return Mono.empty();
                } else {
                  log.info("holders diferente de null?: " + (bankAccounts.getHoldersList() != null));
                  if (bankAccounts.getHoldersList() == null) {
                    log.info("El cliente no posee información de sus holders");
                    return Mono.empty();
                  } else if (bankAccounts.getHoldersList().size() > 0) {
                    bankAccountEventsService.publish(bankAccounts);
                    return bankAccountRepository.save(bankAccounts);
                  } else {
                    log.info(
                        "el cliente no cumple condiciones para la creacion de su cuenta de ahorros");
                    return Mono.empty();
                  }
                }
              });
    }
  }

  @Override
  public Mono<BankAccounts> saveChecking(BankAccounts bankAccounts) {
    if (bankAccounts.getTypeOfClient().equals("personal")
        && bankAccounts.getHoldersList().size() > 2) {
      return bankAccountRepository
          .hasCheckingAccount(bankAccounts.getClientDocument(), "cuenta corriente")
          .flatMap(
              hasCheckings -> {
                if (hasCheckings) {
                  return Mono.empty();
                } else {
                  return bankAccountRepository.save(bankAccounts);
                }
              });
    } else {
      return bankAccountRepository.save(bankAccounts);
    }
  }

  public static BigDecimal retirements(BigDecimal total, BigDecimal retirement) {
    return total.subtract(retirement);
  }

  public static BigDecimal deposits(BigDecimal total, BigDecimal deposit) {
    return total.add(deposit);
  }

  @Override
  public Mono<BankAccounts> registerRetirements(String clientId, BankAccounts bankAccounts) {

    return findById(clientId)
        .flatMap(
            result -> {
              if (bankAccounts.getTypeOfBankAccount().equals("ahorro")
                  && bankAccounts.getTotalTransactionsMonth() == 20) {
                return Mono.empty();
              } else {
                BigDecimal finaAmount =
                    retirements(result.getTotalAmount(), bankAccounts.getTotalAmount());
                bankAccounts.setTotalAmount(finaAmount);
                bankAccounts.setTotalTransactionsMonth(
                    bankAccounts.getTotalTransactionsMonth() + 1);
                LocalDate dateActual = LocalDate.now();
                if (dateActual.getDayOfMonth() == 30) {
                  bankAccounts.setTotalTransactionsMonth(0);
                }
                return bankAccountRepository.save(bankAccounts);
              }
            });
  }

  @Override
  public Mono<BankAccounts> registerDeposits(String clientId, BankAccounts bankAccounts) {
    return findById(clientId)
        .flatMap(
            result -> {
              if (bankAccounts.getTypeOfBankAccount().equals("ahorro")
                  && bankAccounts.getTotalTransactionsMonth() == 20) {
                return Mono.empty();
              } else {
                BigDecimal finalAmount =
                    deposits(result.getTotalAmount(), bankAccounts.getTotalAmount());
                bankAccounts.setTotalAmount(finalAmount);
                bankAccounts.setTotalTransactionsMonth(
                    bankAccounts.getTotalTransactionsMonth() + 1);
                LocalDate dateActual = LocalDate.now();
                if (dateActual.getDayOfMonth() == 30) {
                  bankAccounts.setTotalTransactionsMonth(0);
                }
                return bankAccountRepository.save(bankAccounts);
              }
            });
  }

  @Override
  public Mono deleteById(String clientId) {
    return bankAccountRepository.deleteById(clientId);
  }

  @Override
  public Mono<BankAccounts> saveFixedTerms(BankAccounts bankAccounts) {
    LocalDate date = LocalDate.now();
    if (bankAccounts.getTypeOfClient().equals("empresarial")) {
      return Mono.empty();
    } else {
      if (bankAccounts.getHoldersList().size() > 2) {
        return Mono.empty();
      } else if (date.getDayOfMonth() == 25) {
        return bankAccountRepository.save(bankAccounts);
      } else {
        return Mono.empty();
      }
    }
  }

  @Override
  public Mono<BankAccounts> findById(String clientId) {
    return this.bankAccountRepository.findById(clientId);
  }

  @Override
  public Mono<Boolean> existById(String clientId) {
    return this.bankAccountRepository.existsById(clientId);
  }
}
