package com.bootcamp.domain;

import java.util.Date;
import java.util.UUID;

import com.bootcamp.events.BankAccountCreatedEvent;
import com.bootcamp.events.Event;
import com.bootcamp.events.EventType;
import com.bootcamp.infraestructure.document.BankAccounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class BankAccountEventsService {

  @Autowired private KafkaTemplate<String, Event<?>> producer;

  @Value("${topic.bankAccount.name:bankAccounts}")
  private String topicBankAccount;

  public void publish(BankAccounts bankAccounts) {
    BankAccountCreatedEvent createdEvent = new BankAccountCreatedEvent();
    createdEvent.setData(bankAccounts);
    createdEvent.setId(UUID.randomUUID().toString());
    createdEvent.setType(EventType.CREATED);
    createdEvent.setDate(new Date());

    this.producer.send(topicBankAccount, createdEvent);
  }
}
