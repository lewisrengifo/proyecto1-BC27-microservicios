package com.bootcamp.application;

import com.bootcamp.infraestructure.dto.CreditsRequest;
import com.bootcamp.infraestructure.service.CreditsService;
import com.bootcamp.infraestructure.document.Credits;

import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** Rest controller for credits. */
@RestController
@RequestMapping("/credits")
public class CreditsController {

  @Autowired private CreditsService creditsService;

  @PostMapping("/save")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Credits> registerCredit(@RequestBody CreditsRequest credits) {
    return creditsService.save(credits);
  }

  @PutMapping("/payments/{id}")
  public Mono<Credits> registerPayment(
      @PathVariable String id, @RequestBody CreditsRequest creditsRequest) {
    return creditsService.registerPayments(id, creditsRequest);
  }

  @PutMapping("/consumption/{amount}")
  public Mono<Credits> registerConsumption(
      @PathVariable("amount") BigDecimal amount, Credits credits) {
    return creditsService.registerConsumption(credits, amount);
  }

  @GetMapping("/getAll")
  public Flux<Credits> readCredits() {
    return creditsService.getAll();
  }

  @DeleteMapping("/delete/{id}")
  public Mono<Credits> deleteById(@PathVariable("id") String id) {
    return creditsService.deleteById(id);
  }
}
