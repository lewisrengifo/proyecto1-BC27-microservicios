package com.bootcamp.application;

import com.bootcamp.infraestructure.document.Clients;
import com.bootcamp.infraestructure.dto.ClientsRequest;
import com.bootcamp.infraestructure.service.ClientsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Rest controller for clients.
 */
@RestController
@RequestMapping("/clients")
public class ClientsController {

  @Autowired private ClientsService clientsService;

  @PostMapping
  public Mono<Clients> register(@RequestBody ClientsRequest clients) {
    return clientsService.save(clients);
  }
}
