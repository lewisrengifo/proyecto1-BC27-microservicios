package com.bootcamp.application;

import com.bootcamp.infraestructure.document.BankAccounts;
import com.bootcamp.infraestructure.dto.BankAccountRequest;
import com.bootcamp.infraestructure.service.BankAccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * BankAccountController for product bank accounts.
 */
@RestController
@RequestMapping("/bankAccount")
public class BankAccountController {

  @Autowired private BankAccountsService bankAccountsService;

  @PostMapping("/savings")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<BankAccounts> registerSavings(@RequestBody BankAccountRequest bankAccountsReq) {
    return bankAccountsService.saveSavings(bankAccountsReq);
  }

  @PostMapping("/checkings")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<BankAccounts> registerChecking(@RequestBody BankAccountRequest bankAccounts) {
    return bankAccountsService.saveChecking(bankAccounts);
  }

  @PutMapping("/retirement/{id}")
  public Mono<BankAccounts> registerRetirement(
      @PathVariable("id") String id, @RequestBody BankAccountRequest bankAccounts) {
    return bankAccountsService.registerRetirements(id, bankAccounts);
  }

  @PutMapping("/deposits/{id}")
  public Mono<BankAccounts> registerDeposits(
      @PathVariable("id") String id, @RequestBody BankAccountRequest bankAccounts) {
    return bankAccountsService.registerDeposits(id, bankAccounts);
  }

  @GetMapping("/find")
  public Flux<BankAccounts> readBankAccounts() {
    return bankAccountsService.getAll();
  }

  @GetMapping("/findById/{id}")
  public Mono<BankAccounts> findById(@PathVariable("id") String id) {
    return bankAccountsService.findById(id);
  }

  @DeleteMapping("delete/{id}")
  public Mono<BankAccounts> deleteById(@PathVariable("id") String id) {
    return bankAccountsService.deleteById(id);
  }

  @PostMapping("/fixedTerms")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<BankAccounts> saveFixedTerms(@RequestBody BankAccountRequest bankAccounts) {
    return bankAccountsService.saveFixedTerms(bankAccounts);
  }
}
